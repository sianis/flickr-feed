package hu.sianis.flickrfeed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.PauseOnScrollListener;

import org.json.JSONObject;

import java.util.List;

public class MainActivity extends ActionBarActivity {

    private GridView grid;
    private LoadMoreOnScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        grid = (GridView) findViewById(R.id.grid);
        //Reload the adapter after orientation change
        if (getLastCustomNonConfigurationInstance() != null && getLastCustomNonConfigurationInstance() instanceof GalleryAdapter) {
            grid.setAdapter((GalleryAdapter) getLastCustomNonConfigurationInstance());
        } else {
            grid.setAdapter(new GalleryAdapter(grid.getContext(), android.R.layout.simple_gallery_item, RestHelper.getImages()));
        }
        //Set scroll listeners for image loader and network broadcast receiver
        scrollListener = new LoadMoreOnScrollListener(MainActivity.this, (ArrayAdapter) grid.getAdapter());
        grid.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true, scrollListener));
    }

    public class GalleryAdapter extends ArrayAdapter<JSONObject> {

        public GalleryAdapter(Context context, int resource, List<JSONObject> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView != null) {
                //Recycle image
                holder = (ViewHolder) convertView.getTag();
                //Stop loading task for invisible and not yet used image
                ImageLoader.getInstance().cancelDisplayTask(holder.image);
            } else {
                //Good old viewholder pattern
                convertView = getLayoutInflater().inflate(R.layout.listitem_gallery, parent, false);
                holder = new ViewHolder();
                holder.image = (ImageView) convertView.findViewById(android.R.id.icon);
                convertView.setTag(holder);
            }

            //Start loader task for image
            ImageLoader.getInstance().displayImage(RestHelper.getUrlForImage(getItem(position)), holder.image);
            return convertView;
        }
    }

    private static class ViewHolder {
        ImageView image;
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        //Save adapter for save scroll state and loaded images on orientation change
        if (grid.getAdapter() != null) {
            return grid.getAdapter();
        } else {
            return super.onRetainCustomNonConfigurationInstance();
        }
    }

    //BroadcastReceiver for load images after user turned on network and no images loaded, so scroll listener can't do it
    BroadcastReceiver mNetworkListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (cm.getActiveNetworkInfo().isConnected() && RestHelper.getImages().size() == 0) {
                    //Load images
                    if (grid != null && scrollListener != null) {
                        //This will fire the loader
                        scrollListener.onScroll(grid, 0, 0, 0);
                    }
                }
            } catch (Exception e) {
                //Nothing to do
            }
        }
    };

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //Register listener
        registerReceiver(mNetworkListener, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Unregister listener
        unregisterReceiver(mNetworkListener);
    }
}
