package hu.sianis.flickrfeed;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class FlickrApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //Init rest helper
        RestHelper.init();

        // Create global configuration and initialize ImageLoader with this configuration
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .imageScaleType(ImageScaleType.NONE) //There are fixed size items, so no scale is necessary
                .cacheInMemory(true) //Use memory cache for faster image reloading
                .cacheOnDisc(true) //Use disc cache cause they ask for it
                .showImageOnFail(R.drawable.ic_picture)
                .showImageOnLoading(R.drawable.ic_picture)
                .displayer(new FadeInBitmapDisplayer(500))
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(config);
    }
}
