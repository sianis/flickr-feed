package hu.sianis.flickrfeed;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class RestHelper {

    private static final String FEED_URL = "http://api.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=%s&per_page=100&page=%d&format=json&nojsoncallback=1";
    private static final String IMAGE_URL = "http://farm%d.staticflickr.com/%s/%s_%s_q.jpg";
    private static final String API_KEY = "9dbbc77223964b5963a2aadfd5867106";
    private static AsyncHttpClient client;
    //Store here next page
    private static int page;
    //Count already loaded pages
    private static int loadedPage;
    //Store here all pages, coming from response
    private static int allPage;
    //Store here all images, for adapter
    private static ArrayList<JSONObject> images;

    /*
    Initialize variables when Application start
     */
    public static void init() {
        client = new AsyncHttpClient();
        page = 1;
        loadedPage = -1;
        allPage = -1;
        images = new ArrayList<JSONObject>();
    }

    /**
     * Load next page from feed and send correct events for handler
     *
     * @param responseHandler, optional handler for listening loading process
     */
    public static void loadNextPage(final JsonHttpResponseHandler responseHandler) {
        client.get(String.format(FEED_URL, API_KEY, page), null, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (responseHandler != null) {
                    responseHandler.onStart();
                }
            }

            @Override
            public void onSuccess(JSONObject response) {
                super.onSuccess(response);

                try {
                    if ("ok".equals(response.getString("stat"))) {
                        //Increment to next page
                        page = response.getJSONObject("photos").getInt("page") + 1;
                        //Get current page
                        loadedPage = response.getJSONObject("photos").getInt("page");
                        //Get all pages
                        allPage = response.getJSONObject("photos").getInt("pages");

                        //Parse images into the arraylist
                        JSONArray array = response.getJSONObject("photos").getJSONArray("photo");
                        int length = array.length();
                        for (int i = 0; i < length; i++) {
                            images.add(array.getJSONObject(i));
                        }

                        if (responseHandler != null) {
                            responseHandler.onSuccess(response);
                        }
                    } else {
                        onFailure(response.toString(), new Exception());
                    }
                } catch (Exception e) {
                    onFailure(response.toString(), new Exception());
                }
                if (responseHandler != null) {
                    //Send finish, here, cause we need to hide loader at this point,
                    // not when original listener call onFinish()
                    responseHandler.onFinish();
                }
            }

            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, java.lang.Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                if (responseHandler != null) {
                    responseHandler.onFailure(statusCode, headers, responseBody, error);
                    //Send finish, here, cause we need to hide loader at this point,
                    // not when original listener call onFinish()
                    responseHandler.onFinish();
                }
            }
        });
    }

    /**
     * Determine if feed has more pages
     *
     * @return true if not all page is loaded, false if not all or no page is loaded
     */
    public static boolean hasMorePage() {
        return allPage == -1 || loadedPage < allPage;
    }

    /**
     * @return List of JSONObject of images already fetch from feed
     */
    public static ArrayList<JSONObject> getImages() {
        return images;
    }

    /**
     * Resolve the correct url for image JSON
     *
     * @param image JSONObject contains farm, server, id and secret members for resolving
     * @return Url for image or null
     */
    public static String getUrlForImage(JSONObject image) {
        String ret = null;
        try {
            ret = String.format(IMAGE_URL, image.getInt("farm"), image.getString("server"), image.getString("id"), image.getString("secret"));
        } catch (Exception e) {
            ret = null;
        }
        return ret;
    }
}
