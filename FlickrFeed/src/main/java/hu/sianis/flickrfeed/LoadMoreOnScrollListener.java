package hu.sianis.flickrfeed;

import android.app.Activity;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;

import com.devspark.appmsg.AppMsg;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.util.concurrent.atomic.AtomicBoolean;

public class LoadMoreOnScrollListener implements AbsListView.OnScrollListener {

    //Store is loading already started
    private AtomicBoolean loadingMore = new AtomicBoolean(false);
    //Activity for display messages
    private Activity activity = null;
    //ListAdapter to notify when new items loaded
    private ArrayAdapter listAdapter = null;
    //Messages
    private AppMsg loadingMsg;
    private AppMsg errorMsg;

    LoadMoreOnScrollListener(Activity activity, ArrayAdapter listAdapter) {
        this.listAdapter = listAdapter;
        if (activity != null) {
            loadingMsg = AppMsg.makeText(activity, R.string.loading, AppMsg.STYLE_INFO);
            loadingMsg.setFloating(true);
            errorMsg = AppMsg.makeText(activity, R.string.error, AppMsg.STYLE_ALERT);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        //Do nothing
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int lastInScreen = firstVisibleItem + visibleItemCount;
        if ((lastInScreen == totalItemCount) && !(loadingMore.get()) && RestHelper.hasMorePage() == true) {
            loadingMore.set(true);
            RestHelper.loadNextPage(new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    //Show loader message
                    loadingMsg.show();
                }

                @Override
                public void onSuccess(JSONObject response) {
                    //Notify list to refresh
                    if (listAdapter != null) {
                        listAdapter.notifyDataSetChanged();
                    }
                    loadingMore.set(false);
                }

                @Override
                public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, java.lang.Throwable error) {
                    //Hide loader message and show error
                    loadingMore.set(false);
                    if (loadingMsg.isShowing()) {
                        loadingMsg.cancel();
                    }
                    errorMsg.show();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    //Hide loader message
                    loadingMsg.cancel();
                }
            });
        }
    }
}
